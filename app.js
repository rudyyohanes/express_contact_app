const express = require("express");
const expressLayouts = require("express-ejs-layouts");
const { loadContact, findContact } = require("./utils/contacts");

const app = express();
const port = 3000;

app.set("view engine", "ejs");
// Third-party middleware
app.use(expressLayouts);

// Built-in middleware
app.use(express.static("public"));

app.get("/", (req, res) => {
  // res.sendFile("./index.html", { root: __dirname });
  const student = [
    {
      name: "Rudy Yohanes",
      email: "rudy@gmail.com",
    },
    {
      name: "Angel Gabriel",
      email: "angel@gmail.com",
    },
    {
      name: "Lucy Walter",
      email: "lucy@gmail.com",
    },
  ];
  res.render("index", { name: "Rudy Yohanes", title: "Home Page", student, layout: "layouts/main-layout" });
});

app.get("/about", (req, res) => {
  res.render("about", {
    layout: "layouts/main-layout",
    title: "About Page",
  });
});

app.get("/contact", (req, res) => {
  const contacts = loadContact();

  res.render("contact", {
    layout: "layouts/main-layout",
    title: "Contact Page",
    contacts,
  });
});

app.get("/contact/:name", (req, res) => {
  const contact = findContact(req.params.name);

  res.render("detail", {
    layout: "layouts/main-layout",
    title: "Contact Detail Page",
    contact,
  });
});

app.use("/", (req, res) => {
  res.status(404);
  res.send("<h1>404</h1>");
});

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
});
